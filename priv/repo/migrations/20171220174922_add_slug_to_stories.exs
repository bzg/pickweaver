defmodule Pickweaver.Repo.Migrations.AddSlugToStories do
  use Ecto.Migration

  def change do
    alter table("stories") do
      add :slug, :string, null: false
    end
  end
end
