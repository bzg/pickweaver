defmodule Pickweaver.Repo.Migrations.AddHeaderToStories do
  use Ecto.Migration

  def up do
    alter table("stories") do
      add :header, :string, default: nil
    end
  end

  def down do
    alter table("stories") do
      remove :header
    end
  end
end
