defmodule Pickweaver.Slug do
  @moduledoc """
  Common functions for Slug
  """
  def increment_slug(slug) do
    case List.pop_at(String.split(slug, "-"), -1) do
      {suffix, slug_parts} ->
        case Integer.parse(suffix) do
          {id, _} -> Enum.join(slug_parts, "-") <> "-" <> Integer.to_string(id + 1)
          :error -> slug <> "-1"
        end
      {nil, slug_parts} ->
        slug
    end
  end
end
