defmodule Pickweaver.Services.Parser.Text do
  @moduledoc """
  Parser for inline text (markdown)
  """

  alias Pickweaver.Services.Parser.Text

  defstruct [:text, :force_url]

  def parse_list_with_index(return_list, texts) do
    elements = Enum.map(texts, fn {element, index} -> %Text{text: element.data} end)

    map_elements_to_positions(elements, texts, return_list)
  end

  defp map_elements_to_positions([head|tail], data_list, return_list) do
    {_, index} = Enum.find(data_list, fn {elem, index} -> elem.data == head.text end)
    List.replace_at(map_elements_to_positions(tail, data_list, return_list), index, head)
  end

  defp map_elements_to_positions([], data_list, return_list) do
    return_list
  end
end
