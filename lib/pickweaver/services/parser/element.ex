defmodule Pickweaver.Services.Parser.Element do
  @moduledoc """
  Default element that will be converted to others
  """

  defstruct data: %{}, type: ""
end
