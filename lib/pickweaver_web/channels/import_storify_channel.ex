defmodule PickweaverWeb.ImportStorifyChannel do
  @moduledoc """
  Channel to import Storify Stories
  """
  use Phoenix.Channel
  alias Pickweaver.Services.Import.Storify
  alias Pickweaver.Stories
  alias Pickweaver.Stories.Story
  alias Pickweaver.Import
  alias Pickweaver.Imports
  alias Pickweaver.Repo
  alias Pickweaver.Accounts
  alias PickweaverWeb.ImportStorifyChannel
  import Logger

  def join("import:storify:" <> username, _message, socket) do
    {:ok, socket}
  end

  def handle_in("profile", %{"body" => body}, socket) do
    Logger.debug("Get profile")
    Task.start(Storify, :list_from_account_name, [self(), body["account_name"]])
    {:ok, stories, page} = wait_for_stories_list(socket)
    broadcast! socket, "return_profile", %{stories: stories, page_total: page}
    {:reply, :ok, socket}
  end

  defp wait_for_stories_list(socket, previous_stories \\ []) do
    receive do
      {:fetched_page, stories, page} ->
        broadcast! socket, "fetched_page", %{stories: stories,page: page}
        wait_for_stories_list(socket, previous_stories ++ stories)
      {:finished, stories, page} ->
        {:ok, previous_stories ++ stories, page}
    end
  end

  def handle_in("import", %{"body" => body}, socket) do
    stories_to_import = body["stories"]
    stories_to_import = save_stories(stories_to_import, socket)
    broadcast! socket, "saved_imports", %{}
    process_imports(stories_to_import, socket)
  end

  defp process_imports([head | tail], socket) do
    mark_import_as(head, 1)
    user_id = socket.assigns.user.id
    Task.start(ImportStorifyChannel, :process_import, [self(), head])
    receive do
      {:ok, story, url} ->
        remove_import(url)
        broadcast! socket, "processed_story", %{story: %{slug: story.slug, url: url, status: :ok}}
      {:error, url} ->
        mark_import_as(head, 2)
        broadcast! socket, "processed_story", %{story: %{url: url, status: :failure}}
    end
    process_imports(tail, socket)
  end

  defp process_imports([], socket) do
    broadcast! socket, "finished", %{}
    {:reply, :ok, socket}
  end

  defp remove_import(url) do
    url
    |> Imports.get_import_by_url()
    |> Imports.delete_import()
  end

  def process_import(parent, %Import{} = import) do
    params = Storify.import_from_url(import.url)
    params = Map.put(params, :elements, process_elements(params.elements))
    params = Map.put(params, :account_id, import.account.id)
    case Stories.create_story(params) do
      {:ok, %Story{} = story} ->
        Logger.debug(inspect Map.keys(story), pretty: true)
        send(parent, {:ok, story, import.url})
      {:error, error} ->
        Logger.error(inspect error)
        send(parent, {:error, import.url})
    end
  end

  defp process_elements(elements) do
    Enum.map(elements, fn element ->
      Logger.debug("inspecting elements that will be created")
      Logger.debug(inspect element, pretty: true)
      element = Stories.create_or_return_element(element)
      element.id
    end)
  end

  defp save_stories([head | tail], socket) do
    [save_import(head, socket.assigns.user.id)] ++ save_stories(tail, socket)
  end

  defp save_stories([], socket) do
    broadcast! socket, "saved_stories", %{}
    []
  end

  defp save_import(story, user_id) do
    attrs = %{"url" => story["url"], "account_id" => user_id, "status" => 0}
    {:ok, import} = Imports.create_import(attrs)
    import
  end

  defp mark_import_as(import, status) do
    args = %{status: status}
    if (status == 1) do
      Map.put(args, :tentatives, import.tentatives + 1)
    end
    Imports.update_import(import, args)
  end
end
