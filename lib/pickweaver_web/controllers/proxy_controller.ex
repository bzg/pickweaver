defmodule PickweaverWeb.ProxyController do
  use PickweaverWeb, :controller
  use PhoenixSwagger

  @accepted_mime_types [
    "image/bmp", "image/cgm", "image/g3fax", "image/gif", "image/ief", "image/jp2", "image/jpeg", "image/jpg", "image/pict", "image/png", "image/prs.btif", "image/svg+xml", "image/tiff", "image/vnd.adobe.photoshop", "image/vnd.djvu", "image/vnd.dwg", "image/vnd.dxf", "image/vnd.fastbidsheet", "image/vnd.fpx", "image/vnd.fst", "image/vnd.fujixerox.edmics-mmr", "image/vnd.fujixerox.edmics-rlc", "image/vnd.microsoft.icon", "image/vnd.ms-modi", "image/vnd.net-fpx", "image/vnd.wap.wbmp", "image/vnd.xiff", "image/webp", "image/x-cmu-raster", "image/x-cmx", "image/x-icon", "image/x-macpaint", "image/x-pcx", "image/x-pict", "image/x-portable-anymap", "image/x-portable-bitmap","image/x-portable-graymap", "image/x-portable-pixmap", "image/x-quicktime", "image/x-rgb", "image/x-xbitmap", "image/x-xpixmap", "image/x-xwindowdump"]

  @default_security_headers [{"X-Frame-Options", "deny"}, {"X-XSS-Protection", "1; mode=block"}, {"X-Content-Type-Options", "nosniff"}, {"Content-Security-Policy", "default-src 'none'; img-src data:; style-src 'unsafe-inline'"}, {"Strict-Transport-Security", "max-age=31536000; includeSubDomains"}]

  swagger_path :sign_in do
    summary "Proxify asset"
    description "Proxify asset"
    parameters do
      url :body, :string, "The hashed url", required: true
      hmac :body, :string, "The signature of the request", required: true
    end
    response 200, "Ok", "Image"
  end
  @doc """
  Returns a proxified picture
  """
  def proxify(conn, %{"url" => url, "signature" => signature} = _params) do
    {:ok, url} = Base.decode64(url)
    with true <- check_signature(url, signature),
      {:ok, %HTTPoison.Response{} = response} = HTTPoison.get(url, ["User-Agent": "Pickweaver Image Proxy/1.0"], [follow_redirect: true, max_redirect: 4, ssl: [{:versions, [:'tlsv1.2']}]]),
         {"Content-Type", content_type} <- List.keyfind(response.headers, "Content-Type", 0),
            true <- Enum.member?(@accepted_mime_types, content_type) do
              conn
              |> merge_resp_headers(@default_security_headers)
              |> merge_resp_headers([{"cache-control", "public, max-age=31536000"}])
              |> Plug.Conn.send_resp(200, response.body)
    else
      false ->
        Plug.Conn.send_resp(conn, 400, "The signature was invalid, or content type isn't a picture")
      _ ->
        Plug.Conn.send_resp(conn, 400, "Request failed")
    end
  end

  @doc """
  Generates a url for a secure endpoint
  """
  def gen_params(url) when url != nil do
    secret_key = Application.get_env(:pickweaver, PickweaverWeb.Endpoint)[:secret_key_base]
    hmac = :crypto.hmac(:sha256, secret_key, url)
           |> Base.encode16
           |> String.downcase
    {hmac, Base.encode64(url)}
  end

  def gen_params(nil) do
    nil
  end

  defp check_signature(url, signature) do
    {signature, Base.encode64(url)} === gen_params(url)
  end

  def gen_url(url) do
    case gen_params(url) do
      {hmac, encoded_url} ->
        PickweaverWeb.Router.Helpers.proxy_url(PickweaverWeb.Endpoint, :proxify, encoded_url, hmac)
      nil ->
        nil
    end
  end

end
