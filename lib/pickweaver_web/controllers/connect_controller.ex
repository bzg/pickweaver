defmodule PickweaverWeb.ConnectController do
  @moduledoc """
  Handles the calls to external services APIs to search for elements
  """

  use PickweaverWeb, :controller

  alias Pickweaver.Accounts
  alias Pickweaver.Accounts.Token
  alias Pickweaver.Services.Parser.Tweet

  import Logger

  @doc """
  Returns twitter favourites
  """
  def twitter_fav(conn, _params) do
    account = Guardian.Plug.current_resource(conn)
    configure_ex_twitter(account)
    tweets = ExTwitter.favorites() |> Enum.map(fn tweet -> process_simple_tweet(Tweet.process_tweet(tweet)) end)
    json(conn, %{tweets: tweets})
  end

  @doc """
  Returns tweets for search
  """
  def twitter_search(conn, %{"search" => search} = _params) do
    account = Guardian.Plug.current_resource(conn)
    configure_ex_twitter(account)
    tweets = ExTwitter.search(search, [count: 20]) |> Enum.map(fn tweet -> process_simple_tweet(Tweet.process_tweet(tweet)) end)
    json(conn, %{tweets: tweets})
  end

  @doc """
  Returns tweets for profile
  """
  def twitter_profile(conn, %{"profile" => profile}) do
    account = Guardian.Plug.current_resource(conn)
    configure_ex_twitter(account)
    do_twitter_profile(conn, profile)
  end

  @doc """
  Returns tweets for own profile
  """
  def twitter_profile(conn, _params) do
    account = Guardian.Plug.current_resource(conn)
    profile = configure_ex_twitter(account)
    do_twitter_profile(conn, profile)
  end

  defp do_twitter_profile(conn, profile) do
    tweets = ExTwitter.user_timeline([screen_name: profile, count: 20]) |> Enum.map(fn tweet -> process_simple_tweet(Tweet.process_tweet(tweet)) end)
    json(conn, %{tweets: tweets})
  end

  @doc """
  Proceed to Twitter Auth
  """
  def twitter_auth(conn, %{"callback" => callback}) do
    token = ExTwitter.request_token(callback)
    {:ok, authenticate_url} = ExTwitter.authenticate_url(token.oauth_token)
    json(conn, %{authenticate_url: authenticate_url})
  end

  @doc """
  Callback route called when Twitter Auth succeeds
  """
  def twitter_auth_callback(conn, %{"oauth_token" => oauth_token, "oauth_verifier" => oauth_verifier}) do
    {:ok, access_token} = ExTwitter.access_token(oauth_verifier, oauth_token)
    with {:ok, %Token{} = token} <- Accounts.create_token(%{"service" => "twitter", "data" => %{"access_token" => access_token}, "account_id" => Guardian.Plug.current_resource(conn).id }) do
      json(conn, %{status: :ok})
    end
  end

  @doc """
  Returns if we have twitter authentification tokens for the current account
  """
  def twitter_has_token(conn, _params) do
    account = Guardian.Plug.current_resource(conn)
    case Accounts.get_token(account, "twitter") do
      {:ok, _} ->
        json(conn, %{status: :ok})
      _ ->
        json(conn, %{status: :missing})
    end
  end

  defp configure_ex_twitter(account) do
    with {:ok, %Token{data: data}} <- Accounts.get_token(account, "twitter") do
      [consumer_key: consumer_key, consumer_secret: consumer_secret, access_token: _, access_token_secret: _] = ExTwitter.configure()
      ExTwitter.configure(
        :process,
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token: data["access_token"]["oauth_token"],
        access_token_secret: data["access_token"]["oauth_token_secret"],
      )
      data["screen_name"]
    end
  end

  defp process_simple_tweet(%Tweet{} = tweet) do
    Map.put(tweet, :user, %{username: tweet.user.name, screen_name: tweet.user.screen_name, profile: tweet.user.profile_image_url})
  end
end
