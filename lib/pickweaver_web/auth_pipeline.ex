defmodule PickweaverWeb.AuthPipeline do
  @moduledoc """
  Makes sure the user's logged in
  """

  use Guardian.Plug.Pipeline, otp_app: :pickweaver,
                              module: PickweaverWeb.Guardian,
                              error_handler: PickweaverWeb.AuthErrorHandler

  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource, ensure: true

end
