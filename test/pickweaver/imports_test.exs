defmodule Pickweaver.ImportsTest do
  use Pickweaver.DataCase

  import Pickweaver.Factory

  alias Pickweaver.Imports
  alias Pickweaver.Import

  describe "imports" do

    @valid_attrs %{url: "https://twitter.com/nitot/status/957963447513296897", tentatives: 0}
    @update_attrs %{url: "https://twitter.com/arretsurimages/status/951521164056256513", tentatives: 0}
    @invalid_attrs %{url: nil, tentatives: nil}

    def account_fixture() do
      insert(:account)
    end

    def import_fixture() do
      insert(:import)
    end

    test "list_imports/0 returns all imports" do
      import = import_fixture()
      assert Imports.list_imports() == [import]
    end

    test "list_imports/1 returns the imports for a given account" do
      import = import_fixture()
      _ = import_fixture()
      assert Imports.list_imports(import.account) == [import]
    end

    test "list_imports/1 returns the imports for a given account id" do
      import = import_fixture()
      _ = import_fixture()
      assert Imports.list_imports(import.account.id) == [import]
    end

    test "get_import!/1 returns the import with given id" do
      import = import_fixture()
      assert Imports.get_import_by_url(import.url).url == import.url
    end

    test "create_import/1 with valid data creates a import" do
      account = account_fixture()
      valid_attrs = Map.put(@valid_attrs, :account_id, account.id)
      assert {:ok, %Import{} = import} = Imports.create_import(valid_attrs)
      assert import.url == @valid_attrs.url
    end

    test "create_import/1 with invalid data returns error changeset" do
      account = account_fixture()
      invalid_attrs = Map.put(@invalid_attrs, :account_id, account.id)
      assert {:error, %Ecto.Changeset{}} = Imports.create_import(invalid_attrs)
    end

    test "update_import/2 with valid data updates the import" do
      import = import_fixture()
      assert {:ok, %Import{url: "https://twitter.com/arretsurimages/status/951521164056256513"}} = Imports.update_import(import, @update_attrs)
    end

    test "update_import/2 with invalid data returns error changeset" do
      import = import_fixture()
      account = account_fixture()
      invalid_attrs = Map.put(@invalid_attrs, :account_id, account.id)
      assert {:error, %Ecto.Changeset{}} = Imports.update_import(import, invalid_attrs)
      assert import == Imports.get_import_by_url(import.url)
    end

    test "delete_import/1 deletes the import" do
      import = import_fixture()
      assert {:ok, %Import{}} = Imports.delete_import(import)
      assert_raise Ecto.NoResultsError, fn -> Imports.get_import_by_url(import.url) end
    end
  end
end
