defmodule Pickweaver.AccountsTest do
  use Pickweaver.DataCase

  alias Pickweaver.Accounts

  describe "accounts" do
    alias Pickweaver.Accounts.Account

    @valid_attrs %{email: "foo@bar.tld", password: "some password", role: 42, username: "my account"}
    @update_attrs %{email: "foo@fighters.tld", password: "some updated password", role: 43, username: "some updated user account"}
    @invalid_attrs %{email: nil, password: nil, role: nil, username: nil}

    def account_fixture(attrs \\ %{}) do
      {:ok, account} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_account()

      %{account | password: nil}
    end

    test "list_accounts/0 returns all accounts" do
      account = account_fixture()
      assert Accounts.list_accounts() == [account]
    end

    test "get_account!/1 returns the account with given id" do
      account = account_fixture()
      assert Accounts.get_account!(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      assert {:ok, %Account{} = account} = Accounts.create_account(@valid_attrs)
      assert account.email == "foo@bar.tld"
      assert account.role == 42
      assert account.username == "my account"
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_account(@invalid_attrs)
    end

    test "update_account/2 with valid data updates the account" do
      account = account_fixture()
      assert {:ok, account} = Accounts.update_account(account, @update_attrs)
      assert %Account{} = account
      assert account.email == "foo@fighters.tld"
      assert account.role == 43
      assert account.username == "some updated user account"
    end

    test "update_account/2 with invalid data returns error changeset" do
      account = account_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_account(account, @invalid_attrs)
      assert account == Accounts.get_account!(account.id)
    end

    test "get_account_by_slug/1 with valid data gets the account" do
      account = account_fixture()
      assert account == Accounts.get_account_by_slug(account.slug)
    end

    test "get_account_by_slug/1 with slug for unexistant account returns nil" do
      assert nil == Accounts.get_account_by_slug("invalid slug")
    end

    test "find_by_email/1 with valid data gets the account" do
      account = account_fixture()
      assert account == Accounts.find_by_email(account.email)
    end

    test "find_by_email/1 with slug for unexistant account returns nil" do
      assert nil == Accounts.find_by_email("invalid email")
    end

    test "find_by_username/1 with valid data gets the account" do
      account = account_fixture()
      assert account == Accounts.find_by_username(account.username)
    end

    test "find_by_username/1 with slug for unexistant account returns nil" do
      assert nil == Accounts.find_by_username("invalid username")
    end

    test "authenticate/1 with valid credentials" do
      account = account_fixture()
      assert {:ok, _, _} = Accounts.authenticate(%{username: account, password: @valid_attrs.password})
    end

    test "authenticate/1 with invalid credentials" do
      account = account_fixture()
      assert {:error, :unauthorized} = Accounts.authenticate(%{username: account, password: "bad password"})
    end

    test "delete_account/1 deletes the account" do
      account = account_fixture()
      assert {:ok, %Account{}} = Accounts.delete_account(account)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_account!(account.id) end
    end

    test "change_account/1 returns a account changeset" do
      account = account_fixture()
      assert %Ecto.Changeset{} = Accounts.change_account(account)
    end
  end
end
