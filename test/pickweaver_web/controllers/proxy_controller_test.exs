defmodule PickweaverWeb.ProxyControllerTest do
  use PickweaverWeb.ConnCase
  import Logger

  describe "proxify an url" do
    test "fetch a signed picture url", %{conn: conn} do
      conn = get conn, PickweaverWeb.ProxyController.gen_url("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png")
      assert response(conn, 200)
    end

    test "fetch a signed url that's isn't a picture", %{conn: conn} do
      conn = get conn, PickweaverWeb.ProxyController.gen_url("https://www.google.com")
      assert response(conn, 400)
    end

    test "fetch a signed url with invalid signature", %{conn: conn} do
      {hmac, signed_url} = PickweaverWeb.ProxyController.gen_params("https://google.com")
      conn = get conn, proxy_path(conn, :proxify, signed_url, "toto")
      assert response(conn, 400)
    end
  end

end
