// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex';
import vuexI18n from 'vuex-i18n';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueMoment from 'vue-moment';
import VueClazyLoad from 'vue-clazy-load';
import Snotify, { SnotifyPosition } from 'vue-snotify';
import 'vue-snotify/styles/material.css';
import App from '@/App';
import router from '@/router';
import storeData from '@/store';
import '@/filters';
import { translations, detectLang } from '@/i18n';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(Vuex);
Vue.use(VueMoment);
Vue.use(VueClazyLoad);

const options = {
  toast: {
    position: SnotifyPosition.rightTop,
  },
};

Vue.use(Snotify, options);

const store = new Vuex.Store(storeData);
Vue.use(vuexI18n.plugin, store);

console.log(translations);
Object.entries(translations).forEach((key) => {
  Vue.i18n.add(key[0], key[1]);
});
const userlang = detectLang();
Vue.i18n.set(userlang);
Vue.i18n.fallback('en_US');

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
