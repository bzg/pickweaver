import Vue from 'vue';
import Router from 'vue-router';
import auth from '@/auth/index';
import PageNotFound from '@/components/PageNotFound';
import Home from '@/components/Home';
import MyStories from '@/components/MyStories';
import Stories from '@/components/StoryList';
import Search from '@/components/Search';
import Story from '@/components/story/Story';
// import EditStory from '@/components/story/EditStory';
import NewStory from '@/components/story/NewStory';
import ImportStorifyProfile from '@/components/import/storify/ImportProfile';
import ImportStorifyURL from '@/components/import/storify/ImportURL';
import ImportStorify from '@/components/import/storify/ImportStorify';
import ImportTwitter from '@/components/import/twitter/ImportTwitter';
import ImportTwitterMoment from '@/components/import/twitter/ImportTwitterMoment';
import ImportTwitterTweets from '@/components/import/twitter/ImportTwitterTweets';
import ImportQueue from '@/components/import/Queue';
import Login from '@/components/Login';
import Register from '@/components/Register';
import Profile from '@/components/Profile';
import EditProfile from '@/components/EditProfile';
import Connect from '@/components/connect/Connect';
import ConnectCallback from '@/components/connect/ConnectCallback';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/stories',
      name: 'Stories',
      component: Stories,
    },
    {
      path: '/my-stories/:page?',
      name: 'MyStories',
      component: MyStories,
      props: true,
    },
    {
      path: '/stories/:search_term',
      name: 'Search',
      component: Search,
      props: true,
    },
    {
      path: '/story/:username/:slug/:preview(preview)?',
      name: 'Story',
      component: Story,
      props: true,
    },
    {
      path: '/story/:username/:slug/edit',
      name: 'EditStory',
      component: NewStory,
      props: true,
      meta: { requiresAuth: true },
    },
    {
      path: '/new',
      name: 'NewStory',
      component: NewStory,
      meta: { requiresAuth: true },
    },
    {
      path: '/import/storify',
      name: 'ImportStorify',
      component: ImportStorify,
      meta: { requiresAuth: true },
    },
    {
      path: '/import/storify/url',
      name: 'ImportStorifyURL',
      component: ImportStorifyURL,
      meta: { requiresAuth: true },
    },
    {
      path: '/import/storify/profile',
      name: 'ImportStorifyProfile',
      component: ImportStorifyProfile,
      meta: { requiresAuth: true },
    },
    {
      path: '/import/twitter',
      name: 'ImportTwitter',
      component: ImportTwitter,
      meta: { requiresAuth: true },
    },
    {
      path: '/import/twitter_moment',
      name: 'ImportTwitterMoment',
      component: ImportTwitterMoment,
      meta: { requiresAuth: true },
    },
    {
      path: '/import/twitter_tweets',
      name: 'ImportTwitterTweets',
      component: ImportTwitterTweets,
      meta: { requiresAuth: true },
    },
    {
      path: '/import/queue',
      name: 'ImportQueue',
      component: ImportQueue,
      meta: { requiresAuth: true },
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
    },
    {
      path: '/profile/:slug',
      name: 'Profile',
      component: Profile,
      props: true,
    },
    {
      path: '/profile/:slug/edit',
      name: 'EditProfile',
      component: EditProfile,
      props: true,
      meta: { requiresAuth: true },
    },
    {
      path: '/connect',
      name: 'Connect',
      component: Connect,
    },
    {
      path: '/connect/callback',
      name: 'ConnectCallback',
      component: ConnectCallback,
    },
    { path: '/404',
      name: 'PageNotFound',
      component: PageNotFound,
      meta: { requiresAuth: false },
    },
    { path: '*',
      name: 'PageNotFound',
      redirect: '/404',
    },
  ],
});

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!await auth.isConnected()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router;
