export const API_HOST = '0.0.0.0:4000';
export const API_ORIGIN = `http://${API_HOST}`;
export const API_WEBSOCKET = `ws://${API_HOST}/api`;
export const API_PATH = '/api/v1';
