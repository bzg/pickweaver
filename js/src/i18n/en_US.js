export default {
  pickweaver: {
    app_name: 'Pickweaver',
    desc: 'A free software Storify-clone, currently in beta',
  },
  home: {
    buttons: {
      view_stories: 'View picks',
      add_story: 'Add a pick',
      login: 'Login',
      register: 'Register',
    },
    cards: {
      create_story: {
        title: 'Start by weaving a story',
        paragraph: 'Seekers, followers, and superior powers will always protect them. Quadra, fluctus, et lanista.',
        button: 'Create a story',
      },
      import_storify: {
        title: 'Import your stories from Storify',
        paragraph: 'When the visitor of afterlife shapes the manifestations of the wind, the sainthood will know power.',
        button: 'Import your stories',
      },
    },
    columns: {
      whats: {
        title: 'What\'s this app?',
        paragraph: 'It\'s a Storify-like app to create and publish picks relying on content from external sources, like tweets',
        example: 'You can have a look to <a href="https://pickweaver.tcit.fr/story/mon-histoire">this example</a>',
        sources: 'The sources for this app are available on our Gitlab repository : <a href="https://framagit.org/framasoft/pickweaver">https://framagit.org/framasoft/pickweaver</a>',
      },
      why_name: {
        title: 'Why the name?',
        paragraph: 'It\'s basically italian for "Story". I\'m a really shitty name picker.',
      },
      what_uses: {
        title: 'What did you use to make this?',
        paragraph: '<strike>PHP</strike>. The API server is running with Elixir, with the Phoenix Framework. The front-end uses VueJS and Bootstrap.',
      },
      what_stories: {
        title: 'What are picks?',
        paragraph: 'It\'s like a blog post, but centered on content which is coming from other sources, like tweets or videos. You just need to put some URLs and the content and / or metadata will be fetched automatically.',
        sources_list: {
          paragraph: 'The supported sources are the following:',
          elements: {
            twitter: 'Twitter tweets',
            youtube: 'YouTube videos',
            facebook: 'Facebook (basic)',
            opengraph: 'Any webpage with OpenGraph data',
            basic: 'Any webpage with basic <code>&lt;meta&gt;</code> elements',
          },
          more: 'Of course, more are coming.',
        },
        import: {
          title: 'Import',
          paragraph: 'You can import your picks from the following sources:',
          sources: {
            storify: 'Storify stories (you can import all stories from an user at once',
            twitter: 'Twitter Moments',
          },
        },
      },
      more_questions: {
        title: 'Why, when, who?',
        why: {
          title: 'Why?',
          paragraph: 'Storify <a href="https://storify.com/faq-eol">has announced</a> it would be taken offline on May 16, 2018. And it was quite an useful tool. For some people.',
        },
        when: {
          title: 'When?',
          paragraph: 'This app will be available when it\'s ready ;) However, it\'s already working and available. So I guess it\'s ready.',
        },
        who: {
          title: 'Who?',
          paragraph: 'This app is made by a <a href="https://framasoft.org/">Framasoft</a> employee.',
        },
      },
    },
  },
  navbar: {
    my_lists: 'My lists',
    explore_public_lists: 'Explore public lists',
    search: 'Search',
    profile: 'Profile',
    sign_out: 'Sign out',
    login: 'Login',
  },
  footer: {
    made_by: 'Made by <a href="https://framasoft.org">Framasoft</a> with the support of the <a href="http://www.modernisation.gouv.fr">DINSIC</a>, <a href="http://www.savoirscom1.info" >SavoirsCom1</a> and <a href="https://www.etalab.gouv.fr/">Etalab (EIG)</a>.',
    credits: 'The Needle logo is made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> and <a href="https://www.flaticon.com/" title="Flaticon">flaticon.com</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank" rel="noopener">CC 3.0 BY</a></div>',
    licence: 'Pickweaver is licenced under <a href="https://framagit.org/framasoft/pickweaver">AGPL-3.0</a>',
  },
  import: {
    storify: {
      profile: {
        saved: 'Your imports are processing. Come later to see how it went !',
      },
      single_url: {
        success: 'The Storify Story has been successfully imported',
        failure: 'There was an issue during importing your story',
      },
    },
  },
  my_stories: {
    filter: {
      search: {
        placeholder: 'Search…',
        button: 'Go',
      },
      order_by: {
        label: 'Order your stories:',
        inserted_at: 'Creation date',
        updated_at: 'Update date',
        title: 'Title',
      },
      direction: {
        label: 'Direction:',
        asc: 'ASC',
        desc: 'DESC',
      },
    },
  },
  story: {
    last_updated_on: 'Last updated {time}',
    view_story: 'View story',
    create_story: 'Create story',
    no_stories: 'There\'s no stories',
    form: {
      empty_body: 'Body of your story can\'t be empty',
      invalid: 'Form is invalid',
    },
    actions: {
      edit: 'Edit',
      delete: {
        button: 'Delete',
        warning: {
          title: 'Do you really want to delete this story ?',
          paragraph: '{story} will be deleted',
          button: 'Delete Story',
        },
      },
      publish: {
        publish: 'Publish',
        unpublish: 'Unpublish',
        success_publish: 'Story has been published',
        success_unpublish: 'Story has been unpublished',
      },
    },
    edit: {
      back: 'Back',
      add_text: 'Add text',
      add_line: 'Add line',
      condensed_view: 'Condensed View',
      preview: 'Preview',
      publish: 'Publish',
      cancel: 'Cancel',
      submit: 'Submit',
      saved: {
        success: 'Story has been saved',
      },
    },
  },
  finder: {
    info: {
      search: 'Search elements in the sidebar',
      drag: 'And drag & drop them in your story!',
    },
    twitter: {
      connect: {
        title: 'You need to connect to Twitter to find elements',
        button: 'Connect to my Twitter account',
      },
      favourites: {
        title: 'Favourites',
      },
      search: {
        title: 'Search',
      },
      profile: {
        title: 'Profile',
      },
    },
    instagram: {
      not_available: 'Instagram search is not available yet',
    },
    youtube: {
      not_available: 'YouTube search is not available yet',
    },
    search_engine: {
      not_available: 'Search engine search is not available yet',
    },
  },
  profile: {
    hello: 'Hi {user}',
    no_stories: 'You don\'t have any picks yet, do you want to weave one?',
    no_stories_anon: '{user} hasn\'t published any picks yet',
    disconnect: {
      btn: 'Disconnect',
      success: 'You have been disconnected',
    },
    edit: {
      profile: {
        title: 'Profile',
        success: 'Your profile has been successfully updated',
      },
      account: {
        title: 'Account settings',
        success: 'Your account has been successfully updated',
      },
      password: {
        title: 'Password',
        success: 'Your password has been successfully updated',
      },
    },
    form: {
      display_name: {
        label: 'Displayed name',
        desc: 'The name which will be displayed on your picks and on your profile.',
      },
      username: {
        label: 'Username',
      },
      password: {
        label: 'Password',
        desc: 'Please be strong',
      },
      email: {
        label: 'Email',
        desc: 'Your email will never be shared with anyone',
      },
      avatar: 'Avatar',
      update_btn: 'Update my profile',
      cancel_btn: 'Cancel',
    },
    stats: {
      stories: '{stories} picks published',
    },
  },
  session: {
    error: {
      bad_login: 'Your username or password is incorrect',
    },
    success: {
      login: 'Welcome on Pickweaver, {username}',
    },
    form: {
      username: {
        label: 'Username',
      },
      password: {
        label: 'Password',
        desc: 'Please be strong',
      },
      email: {
        label: 'Email',
        desc: 'Your email will never be shared with anyone',
      },
      login_btn: 'Login',
      cancel_btn: 'Cancel',
    },
  },
  registration: {
    form: {
      email: {
        label: 'Email',
        desc: 'Your email will never be shared with anyone',
      },
      username: {
        label: 'Username',
      },
      password: {
        label: 'Password',
        desc: 'Please be strong',
      },
      register_btn: 'Register',
      cancel_btn: 'Cancel',
    },
    error: {
      email_already_used: 'This email is already in use',
      username_already_used: 'This username is already in use',
      password_too_short: 'Your password is too short, it must be at least 6 caracters',
    },
    success: {
      login: 'Your are now registered on Pickweaver, welcome!',
    },
  },
  errors: {
    404: {
      title: 'Page not found',
    },
  },
};
