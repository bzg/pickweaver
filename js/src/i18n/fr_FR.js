export default {
  pickweaver: {
    app_name: 'Pickweaver',
    desc: 'Un clone libre de Storify, actuellement en version beta',
  },
  home: {
    buttons: {
      view_stories: 'Voir les picks',
      add_story: 'Ajouter un pick',
      login: 'Se connecter',
      register: 'S\'inscrire',
    },
    cards: {
      create_story: {
        title: 'Commencez par créer une story',
        paragraph: 'Seekers, followers, and superior powers will always protect them. Quadra, fluctus, et lanista.',
        button: 'Créer une story',
      },
      import_storify: {
        title: 'Importez vos stories depuis Storify',
        paragraph: 'When the visitor of afterlife shapes the manifestations of the wind, the sainthood will know power.',
        button: 'Importez vos stories',
      },
    },
    columns: {
      whats: {
        title: 'Qu\'est-ce que cette application ?',
        paragraph: 'C\'est une application semblable à Storify permettant de créer et de publier des picks basées sur du contenu provenant de sources externes, comme un tweet.',
        example: 'Vous pouvez jeter un œil à <a href="https://pickweaver.tcit.fr/story/mon-histoire">cet exemple</a>',
        sources: 'Les source pour cette application sont disponibles sur notre dépôt Gitlab : <a href="https://framagit.org/framasoft/pickweaver">https://framagit.org/framasoft/pickweaver</a>',
      },
      why_name: {
        title: 'Pourquoi le nom ?',
        paragraph: 'C\'est simplement la traduction italienne de « Story ». Je suis vraiment nul pour choisir des noms.',
      },
      what_uses: {
        title: 'Qu\'avez-vous utilisé pour créer cette application ?',
        paragraph: '<strike>PHP</strike>. Le serveur API fonctionne sous Elixir, avec le Framework Phoenix. La partie front-end utilise VueJS et Bootstrap.',
      },
      what_stories: {
        title: 'Que sont les picks?',
        paragraph: 'Ce sont comme des posts de blog, mais centrés autour de contenus provenant d\'autres sources, comme des tweets ou des vidéos. Vous avez juste besoin de fournir des URLs et le contenu et/ou les méta-données seront récupérées automatiquement.',
        sources_list: {
          paragraph: 'Les sources supportées sont les suivantes :',
          elements: {
            twitter: 'Les messages sur Twitter (tweets)',
            youtube: 'Les vidéos sur YouTube',
            facebook: 'Les messages Facebook',
            opengraph: 'N\'importe quelle page avec des données OpenGraph',
            basic: 'N\'importe quelle page avec des balises <code>&lt;meta&gt;</code>',
          },
          more: 'Bien entendu, plus de sources seront supportées dans le futur.',
        },
        import: {
          title: 'Import',
          paragraph: 'Vous pouvez importer des picks directement depuis les sources suivantes :',
          sources: {
            storify: 'Les stories disponibles sur Storify (par URL ou bien par nom d\'utilisateur',
            twitter: 'Les messages sur Twitter, à partir d\'une discussion entière ou bien un Moment twitter',
          },
        },
      },
      more_questions: {
        title: 'Pourquoi, quand, qui ?',
        why: {
          title: 'Pourquoi ?',
          paragraph: 'Le service Storify <a href="https://storify.com/faq-eol">a annoncé</a> qu\'il fermerait ses portes le 16 mai 2018. Et c\'était un outil assez utile pour un nombre significatif de personnes.',
        },
        when: {
          title: 'Quand ?',
          paragraph: 'Cette application sera disponible lorsqu\'elle sera prête ;) Toutefois, étant donné qu\'elle est déjà fonctionnelle et disponible, je dirais qu\'elle est prête',
        },
        who: {
          title: 'Qui ?',
          paragraph: 'Cette application est réalisée par un employé de <a href="https://framasoft.org/">Framasoft</a>.',
        },
      },
    },
  },
  navbar: {
    my_lists: 'Mes listes',
    explore_public_lists: 'Explorer les listes publiques',
    search: 'Recherche',
    profile: 'Profil',
    sign_out: 'Se déconnecter',
    login: 'Se connecter',
  },
  footer: {
    made_by: 'Fait par <a href="https://framasoft.org">Framasoft</a> avec le soutien de la <a href="http://www.modernisation.gouv.fr">DINSIC</a>, <a href="http://www.savoirscom1.info" >SavoirsCom1</a> et <a href="https://www.etalab.gouv.fr/">Etalab (EIG)</a>.',
  },
  story: {
    last_updated_on: 'Mis à jour il y a {time}',
    view_story: 'Voir le pick',
    create_story: 'Créer un pick',
    no_stories: 'Il n\'y a pas de picks',
    form: {
      empty_body: 'Body of your story can\'t be empty',
      invalid: 'Form is invalid',
    },
    saved: {
      success: 'La story a été sauvegardée',
    },
  },
  profile: {
    hello: 'Salut {user}',
    no_stories: 'Vous n\'avez pas encore de picks, voulez-vous en créer une ?',
    no_stories_anon: '{user} n\'a pas encore publié de pickweavers',
    form: {
      display_name: {
        label: 'Nom affiché',
        desc: 'Le nom qui sera affiché sur vos picks et votre profil.',
      },
      username: {
        label: 'Nom d\'utilisateur',
      },
      password: {
        label: 'Mot de passe',
        desc: 'Soyez forts',
      },
      email: {
        label: 'Email',
        desc: 'Votre adresse email ne sera jamais partagée avec quiconque',
      },
      avatar: 'Avatar',
      update_btn: 'Mettre à jour mon profil',
      cancel_btn: 'Annuler',
    },
    stats: {
      stories: '{stories} picks publiées',
    },
  },
  session: {
    error: {
      bad_login: 'Votre nom d\'utilisateur ou mot de passe est incorrect',
    },
    success: {
      login: 'Bienvenue sur Pickweaver, {username}',
    },
    form: {
      username: {
        label: 'Nom d\'utilisateur',
      },
      password: {
        label: 'Mot de passe',
        desc: 'Soyez forts',
      },
      email: {
        label: 'Email',
        desc: 'Votre adresse email ne sera jamais partagée avec quiconque',
      },
      login_btn: 'Se connecter',
      cancel_btn: 'Annuler',
    },
  },
  registration: {
    form: {
      email: {
        label: 'Email',
        desc: 'Votre adresse email ne sera jamais partagée avec quiconque',
      },
      username: {
        label: 'Nom d\'utilisateur',
      },
      password: {
        label: 'Mot de passe',
        desc: 'Soyez forts',
      },
      register_btn: 'S\'inscrire',
      cancel_btn: 'Annuler',
    },
    error: {
      email_already_used: 'Cet email est déjà utilisé',
      username_already_used: 'Ce nom d\'utilisateur est déjà utilisé',
      password_too_short: 'Votre mot de passe est trop court, il doit comprendre au moins 6 caractères',
    },
    success: {
      login: 'Vous êtes maintenant inscrit sur Pickweaver, bienvenue !',
    },
  },
};
