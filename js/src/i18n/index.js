/* eslint-disable */
import fr_FR from './fr_FR';
import en_US from './en_US';

const translations = {
  fr_FR, en_US,
};
/* eslint-enable */

const prefix = function prefix(lang) {
  const parts = lang.split('_');
  if (parts.length > 1) {
    return parts[0];
  }
  return lang;
};

const matchPrefix = function matchPrefix(lang) {
  /* eslint-disable */
  for (const translation in translations) {
    if (prefix(translation) === lang) {
      return translation;
    }
  }
  return null;
  /* eslint-enable */
};

const translate = function translate(language, languages) {
  if (Object.prototype.hasOwnProperty.call(translations, language)) {
    return language;
  } else if (matchPrefix(language)) {
    return matchPrefix(language);
  }
  return translate(languages.shift(), languages);
};

function readCookie(name) {
  const nameEQ = `${name}=`;
  const ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

function detectLang() {
  const languages = [];
  const clang = readCookie('lang');
  if (clang !== undefined && clang !== null) {
    languages.push(clang);
  }
  const nl = navigator.languages;
  for (let i = 0; i < nl.length; i += 1) {
    languages.push(nl[i]);
  }
  if (languages.length !== 0) {
    return translate(languages.shift(), languages);
  }
  console.log('Using old style language detection.');
  const userLang = navigator.language || navigator.userLanguage;
  return translate(userLang);
}

export { translations, detectLang };
